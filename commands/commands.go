package commands

import (
	"strings"

	"github.com/morpheusxaut/irc"
	"github.com/mvdan/xurls"
)

func QuickResponse(m *irc.Message, text string) *irc.Message {
	return &irc.Message{
		Command:  irc.PRIVMSG,
		Params:   m.Params,
		Trailing: text,
	}
}

func PrivMsg(channel, message string) *irc.Message {
	return &irc.Message{
		Command:  irc.PRIVMSG,
		Params:   []string{"#" + channel},
		Trailing: message,
	}
}

func Clean(text string) string {
	sa := xurls.Relaxed.FindAllString(text, -1)
	for _, link := range sa {
		println("clean link:", link)
		text = strings.Replace(text, link, "<link removed>", -1)
	}

	return strings.TrimSpace(text)
}
