package main

import (
	"flag"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"time"

	"twitchalertsbot/bot"

	"github.com/naoina/toml"
)

var (
	config string
)

func init() {
	flag.StringVar(&config, "config", "config.toml", "TOML config")
}

type tomlConfig struct {
	Bot bot.Config
}

func main() {
	flag.Parse()
	rand.Seed(time.Now().UTC().UnixNano())

	f, err := os.Open(config)
	if err != nil {
		log.Fatal(err)
	}
	defer func(f *os.File) {
		f.Close()
	}(f)

	buf, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatal(err)
	}
	var config tomlConfig
	if err := toml.Unmarshal(buf, &config); err != nil {
		log.Fatal(err)
	}
	f.Close()

	b, err := bot.NewBot(config.Bot)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("starting...")
	b.Run()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	<-c
	log.Println("exiting...")
	b.Stop()
}
