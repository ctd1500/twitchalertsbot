package bot

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"

	"github.com/boltdb/bolt"
	"golang.org/x/oauth2"
)

func initBolt(db *bolt.DB) error {
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(bucketName)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}

	return err
}

func (b *Bot) Get(key []byte) (val []byte, err error) {
	err = b.Features.Bolt.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(bucketName)
		if bucket == nil {
			return fmt.Errorf("Get: Bucket '%q' not found!", bucketName)
		}

		val = bucket.Get(key)

		return nil
	})
	return val, err
}

func (b *Bot) Put(key, val []byte) error {
	return b.Features.Bolt.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(bucketName)
		if bucket == nil {
			return fmt.Errorf("Put: Bucket '%q' does not exist.", bucketName)
		}
		if err := bucket.Put(key, val); err != nil {
			return err
		}
		return nil
	})
}

func (b *Bot) Delete(key []byte) error {
	return b.Features.Bolt.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(bucketName)
		if bucket == nil {
			return fmt.Errorf("Delete: Bucket '%q' does not exist.", bucketName)
		}
		if err := bucket.Delete(key); err != nil {
			return err
		}
		return nil
	})
}

func (b *Bot) EncodeToken(token *oauth2.Token) ([]byte, error) {
	buf := new(bytes.Buffer)
	end := gob.NewEncoder(buf)
	err := end.Encode(token)
	if err != nil {
		log.Println("encodeToken:", err)
		return nil, err
	}
	return buf.Bytes(), err
}

func (b *Bot) DecodeToken(token []byte) (*oauth2.Token, error) {
	buf := bytes.NewBuffer(token)
	t := new(oauth2.Token)
	dec := gob.NewDecoder(buf)
	err := dec.Decode(t)
	if err != nil {
		log.Println("decodeToken:", err)
		return nil, err
	}
	log.Println("DecodeToken t:", t.AccessToken, " - r:", t.RefreshToken)
	return t, err
}

func (b *Bot) SaveDonationId(id string) error {
	return b.Put(last_donation, []byte(id))
}

func (b *Bot) GetLastDonation() (string, error) {
	id, err := b.Get(last_donation)
	if err != nil {
		log.Println("GetLastDonation Error:", err)
		return "1", err
	}
	return string(id), err
}

func (b *Bot) GetToken() (*oauth2.Token, error) {
	v, err := b.Get(tokenSrcName)
	if err != nil {
		log.Println(err)
	}
	tok, err := b.DecodeToken(v)

	return tok, err
}

func (b *Bot) SaveTokenP() error {
	// TODO:  Make custom RoundTripper to save the tokens instead of grabbing them from time to time?
	// Pull new Token from time to time - https://gist.github.com/billmccord/4247b0c4d2a6b5a4d09f
	token, err := b.OClient.Transport.(*oauth2.Transport).Source.Token()
	if err != nil {
		log.Println("saveToken Error:", err)
		return err
	}

	err = b.SaveToken(token)
	return err
}

func (b *Bot) SaveToken(token *oauth2.Token) error {
	log.Println("SaveToken t:", token.AccessToken, " - r:", token.RefreshToken)
	return b.Features.Bolt.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists(bucketName)
		if err != nil {
			return err
		}

		t := bucket.Get(tokenSrcName)
		if t != nil {
			tk, err := b.DecodeToken(t)
			if err == nil {
				if tk.AccessToken == token.AccessToken && tk.RefreshToken == token.RefreshToken {
					// Tokens are the same, don't waste time updating them.
					return nil
				}
			}
		}

		tok, err := b.EncodeToken(token)
		if err != nil {
			return err
		}

		err = bucket.Put(tokenSrcName, tok)
		if err != nil {
			return err
		}
		return nil
	})
}
