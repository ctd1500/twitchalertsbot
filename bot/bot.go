package bot

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"twitchalertsbot/commands"
	"twitchalertsbot/featureset"
	"twitchalertsbot/irc"

	"github.com/boltdb/bolt"
	"github.com/dukex/squeue"
	"github.com/valyala/fasttemplate"
	"golang.org/x/oauth2"
)

const (
	prefix = "$"
)

var (
	bucketName    = []byte("donationalerts")
	tokenSrcName  = []byte("TokenSrc")
	last_donation = []byte("lastdonation")
)

// Config defines the configuration for a Bot (to be filled in with TOML)
type Config struct {
	Nick       string `toml:"nick"`
	Pass       string `toml:"pass"`
	Channel    string `toml:"channel"`
	ApiCode    string `toml:"apicode"`
	Messages   bool   `toml:"messages"`
	Template   string `toml:"template"`
	StripLinks bool   `toml:"striplinks"`
}

// Bot represents a BotZik IRC bot
type Bot struct {
	Config    Config
	Features  *featureset.FeatureSet
	OClient   *http.Client
	OConf     *oauth2.Config
	CToken    *oauth2.Token
	SchedStop chan bool
	RateQueue *squeue.Queue
}

// TwitchAlerts API Struct
// http://play.golang.org/p/6fChd6KeCg
type taApi struct {
	Donations []Donation `json:"data,omitempty"`
	Error     string     `json:"error"`
	Message   string     `json:"message"`
}

type Donation struct {
	Donation_id string `json:"donation_id"`
	Created_at  string `json:"created_at"`
	Currency    string `json:"currency"`
	Amount      string `json:"amount"`
	Name        string `json:"name"`
	Message     string `json:"message"`
}

// NewBot creates a new Bot.
func NewBot(config Config) (*Bot, error) {
	if err := validateConfig(config); err != nil {
		return nil, err
	}

	db, err := bolt.Open("tabolt.db", 0600, nil)
	if err != nil {
		return nil, err
	}
	initBolt(db)

	return &Bot{
		Config: config,
		Features: &featureset.FeatureSet{
			Twitch: bzirc.NewTwitchChatConn(config.Nick, config.Pass),
			Bolt:   db,
		},
		OConf: &oauth2.Config{
			ClientID:     "",
			ClientSecret: "",
			Scopes:       []string{"donations.read"},
			RedirectURL:  "http://127.0.0.1/",
			Endpoint: oauth2.Endpoint{
				AuthURL:  "https://www.twitchalerts.com/api/v1.0/authorize",
				TokenURL: "https://www.twitchalerts.com/api/v1.0/token",
			},
		},
		RateQueue: squeue.NewQueue(2 * time.Second),
	}, nil
}

func validateConfig(config Config) error {
	switch {
	case config.Nick == "":
		return errors.New("You must specify a nick.")
	case config.Pass == "":
		return errors.New("You must specify a pass.")
	case config.Channel == "":
		return errors.New("You must specify a channel to join.")
	case config.ApiCode == "":
		return errors.New("You must specify a TwitchAlerts API Code.")
	default:
		return nil
	}
}

// Run runs a bot.
func (b *Bot) Run() error {
	log.Println("Init Auth.")
	b.initAuth()
	log.Println("Auth initialized...")

	if err := b.Features.Twitch.Run(); err != nil {
		return err
	}

	if err := b.Features.Twitch.JoinChannel(b.Config.Channel); err != nil {
		return err
	}

	b.RateQueue.Run()

	time.Sleep(13 * time.Second)

	if b.OClient != nil {
		log.Println("initAuth - Initializing API Scheduler...")
		b.initScheduler()
	}

	return nil
}

// Stop stops a bot.
func (b *Bot) Stop() {
	err := b.SaveTokenP()
	if err != nil {
		log.Println("Stop() -> SaveToken Error:", err)
	}
	b.SchedStop <- true
	b.Features.Twitch.Stop()
	err = b.Features.Bolt.Close()
	if err != nil {
		log.Fatal(err)
	}
}

func (b *Bot) initAuth() {
	initial := false
	oclient := Client{
		Config: b.OConf,
		Bot:    b,
	}
	v, err := b.Get(tokenSrcName)
	if err != nil {
		log.Println(err)
	}
	if v == nil {
		initial = true
	}

	if initial {
		tok, err := oclient.Exchange(oauth2.NoContext, b.Config.ApiCode)
		if err != nil {
			log.Println("Error authenticating with API Code, it may have expired. ",
				" Please get a new code and put it into the config file and try again.")
			log.Fatal(err)
		} else {
			log.Println("initAuth initial Exchange Token:", tok.AccessToken, "- expire:", tok.Expiry)
			b.OClient = oauth2.NewClient(oauth2.NoContext, oclient.TokenSource(oauth2.NoContext, tok))
		}
	} else {
		tok, err := b.DecodeToken(v)
		if err != nil {
			er := b.Delete(tokenSrcName)
			if er != nil {
				log.Println("")
			}
			log.Fatal("Unable to fetch Token, new API code may be needed. Error:", err)
		} else {
			log.Println("initAuth saved Exchange Token:", tok.AccessToken, "- expire:", tok.Expiry)
			b.OClient = oauth2.NewClient(oauth2.NoContext, oclient.TokenSource(oauth2.NoContext, tok))
		}
	}
}

func (b *Bot) getDonations() (*taApi, error) {
	ta := &taApi{}
	limit := "5"
	tok := b.CToken
	log.Println("getDonations token:", tok.AccessToken, "- ref:", tok.RefreshToken, "- exp:", tok.Expiry)
	last_id, err := b.GetLastDonation()

	if b.Config.Messages == false {
		limit = "10"
	}

	//token, err := b.OClient.Transport.(*oauth2.Transport).Source.Token()
	resp, err := b.OClient.Get(fmt.Sprintf(
		"https://www.twitchalerts.com/api/v1.0/donations?currency=USD&limit=%v&after=%v&access_token=%v",
		limit, last_id, tok.AccessToken))
	log.Println("getDonations Request: ", resp.Request.URL)
	if err != nil {
		log.Println(err)
		return ta, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return ta, err
	}
	log.Println("body:", string(body))

	err = json.Unmarshal(body, &ta)
	if err != nil {
		log.Println(err)
		return ta, err
	}
	if ta.Donations != nil && len(ta.Donations) > 0 {
		// data
	}

	return ta, err
}

func (b *Bot) donationScheduler() {
	log.Println("donationScheduler running...")
	ta, err := b.getDonations()
	if err != nil {
		log.Println("donationScheduler Error:", err)
	}
	if ta.Error != "" || ta.Message != "" {
		log.Println(fmt.Sprintf("donationScheduler API Error (%v) Msg: %q", ta.Error, ta.Message))
	}
	if ta.Donations != nil && len(ta.Donations) > 0 {
		donArr := []Donation{}
		err := b.SaveDonationId(ta.Donations[0].Donation_id)
		if err != nil {
			log.Println("donationScheduler Save Last Donation Error:", err)
		}
		// reverse the donation array, get/display the oldest donations first
		for i := len(ta.Donations) - 1; i >= 0; i-- {
			v := ta.Donations[i]
			// Store reversed donation array in donArr
			donArr = append(donArr, v)
			println("v msg:", v.Message, "name:", v.Name)
			amnt := v.Amount
			i := strings.Index(amnt, ".")
			n := (i + 3)
			amount := amnt[:n]
			log.Println(fmt.Sprintf("name: %v - amount: %v", v.Name, amount))

			if b.Config.Messages {
				b.AddDonation(v)
			}
		}
		// TODO: use donArr to pass donations to one of the functions.
		// Either a "show 1 donation every 2 seconds with message"
		// or a "show all donations as 'donation 1 | donation 2' if messages are disabled"
		if b.Config.Messages == false {
			b.AddDonations(donArr)
		}
	}
	log.Println("donationScheduler finished.")
}

// Add donations with no message
func (b *Bot) AddDonations(d []Donation) {
	//dfmt := "%v%v donated $%v!%v"
	msgsplitter := " | "
	message := ""
	for i := 0; i < len(d); i++ {
		if i == len(d)-1 {
			msgsplitter = ""
		}
		amnt := d[i].Amount
		si := strings.Index(amnt, ".")
		n := (si + 3)
		amount := amnt[:n]
		message = fmt.Sprintf("%v%v", message, b.parseTemplate(b.Config.Template, true, d[i].Name, amount, msgsplitter))
	}

	log.Println("Sending donations: ", message)
	message = "New Donations: " + message

	b.RateQueue.Push(func() {
		log.Println("RateQueue message:", message)
		b.Features.Twitch.SendChan <- commands.PrivMsg(b.Config.Channel, message)
	})
}

// Add donations with message to be displayed one at a time.
func (b *Bot) AddDonation(d Donation) {
	//dfmt := "%v donated $%v!%v%v"
	//msgfmt := " Message: "
	//msg := ""
	cmsg := ""
	if d.Message != "" {
		//msg = msgfmt
	}
	amnt := d.Amount
	si := strings.Index(amnt, ".")
	n := (si + 3)
	amount := amnt[:n]
	log.Println("Sending donation: ", b.parseTemplate(b.Config.Template, false, d.Name, amount, d.Message))

	if d.Message != "" && b.Config.StripLinks {
		cmsg = commands.Clean(d.Message)
	} else {
		cmsg = d.Message
	}

	message := b.parseTemplate(b.Config.Template, false, d.Name, amount, cmsg)
	//message = "New Donation: " + message
	log.Println("Actual donation: ", message)

	b.RateQueue.Push(func() {
		log.Println("RateQueue message:", message)
		b.Features.Twitch.SendChan <- commands.PrivMsg(b.Config.Channel, message)
	})
}

func (b *Bot) parseTemplate(template string, split bool, name, amount, message string) string {
	msg := " Message: "
	if split == true {
		msg = ""
	}
	if template == "" {
		template = "[name] donated [amount]![message]"
	}
	if message == "" {
		msg = ""
	} else {
		msg = msg + message
	}

	t, err := fasttemplate.NewTemplate(template, "[", "]")
	if err != nil {
		log.Fatalf("unexpected error when parsing template: %s", err)
	}
	s := t.ExecuteString(map[string]interface{}{
		"name":    name,
		"amount":  "$" + amount,
		"message": msg,
	})
	return s
}

func (b *Bot) initScheduler() {
	ping := func() {
		b.donationScheduler()
	}

	b.SchedStop = b.schedule(ping, 12*time.Second)
}

func (b *Bot) schedule(fn func(), delay time.Duration) chan bool {
	stop := make(chan bool)

	go func() {
		for {
			fn()
			select {
			case <-time.After(delay):
			case <-stop:
				return
			}
		}
	}()

	return stop
}
