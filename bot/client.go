package bot

import (
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"log"
	"sync"
)

// https://gist.github.com/agtorre/350c5b4ce0ccebc5ac0f
// https://groups.google.com/forum/#!topic/golang-nuts/5jFjsIg_d4M

type Client struct {
	Config *oauth2.Config
	Bot    *Bot
	mu     sync.Mutex
}

func (c *Client) StoreToken(token *oauth2.Token) error {
	log.Println("oauth StoreToken token:", token.AccessToken, "- refresh:", token.RefreshToken, "- expire:", token.Expiry)
	return c.Bot.SaveToken(token)
}

func (c *Client) SetToken(token *oauth2.Token) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Bot.CToken = token
	return nil
}

func (c *Client) Exchange(ctx context.Context, code string) (*oauth2.Token, error) {
	token, err := c.Config.Exchange(ctx, code)
	if err != nil {
		return nil, err
	}
	if err := c.StoreToken(token); err != nil {
		return nil, err
	}
	c.SetToken(token)
	return token, nil
}

func (c *Client) TokenSource(ctx context.Context, t *oauth2.Token) oauth2.TokenSource {
	rts := &CacheTokenSource{
		source: c.Config.TokenSource(ctx, t),
		client: c,
	}
	c.SetToken(t)
	return oauth2.ReuseTokenSource(t, rts)
}

type CacheTokenSource struct {
	source oauth2.TokenSource
	client *Client
}

func (t *CacheTokenSource) Token() (*oauth2.Token, error) {
	token, err := t.source.Token()
	if err != nil {
		return nil, err
	}
	if err := t.client.StoreToken(token); err != nil {
		return nil, err
	}
	t.client.SetToken(token)
	return token, nil
}
