package featureset

import (
	"twitchalertsbot/irc"

	"github.com/boltdb/bolt"
)

// FeatureSet is a set of "features" like an IRC connection or database
type FeatureSet struct {
	Twitch *bzirc.IRCConn
	Bolt   *bolt.DB
}
