package bzirc

import (
	"io"
	"log"

	"github.com/morpheusxaut/irc"
)

const (
	twitchChatAddr = "irc.twitch.tv:6667"
)

var (
	twitchChatCaps = []string{"twitch.tv/tags"}
)

// IRCConn handles a IRC connection
type IRCConn struct {
	conn         *irc.Conn
	addr         string
	nick         string
	pass         string
	capabilities []string
	channels     map[string]struct{}
	decChan      chan *irc.Message
	RecvChan     chan *irc.Message
	SendChan     chan *irc.Message
	stopChan     chan struct{}
}

// NewIRCConn creates a new IRC connection
func NewIRCConn(addr, nick, pass string) *IRCConn {
	return &IRCConn{
		addr:     addr,
		nick:     nick,
		pass:     pass,
		channels: make(map[string]struct{}),
		decChan:  make(chan *irc.Message),
		RecvChan: make(chan *irc.Message),
		SendChan: make(chan *irc.Message),
		stopChan: make(chan struct{}),
	}
}

// NewTwitchChatConn creates a new Twitch IRC chat connection
func NewTwitchChatConn(nick, pass string) *IRCConn {
	conn := NewIRCConn(twitchChatAddr, nick, pass)
	conn.capabilities = twitchChatCaps
	return conn
}

// Run runs the IRCConn, reading and writing messages as they occur
func (tc *IRCConn) Run() (err error) {
	err = tc.connect()
	if err != nil {
		return
	}

	go tc.run()

	return
}

// Stop closes the IRC connection.
func (tc *IRCConn) Stop() {
	tc.stopChan <- struct{}{}
	<-tc.stopChan
}

func (tc *IRCConn) run() {
	go tc.decoder()

	for {
		select {
		case <-tc.stopChan:
			quitMessage := &irc.Message{
				Command: irc.QUIT,
			}
			tc.conn.Encode(quitMessage)
			tc.stopChan <- struct{}{}
			return
		case m := <-tc.SendChan:
			if err := tc.conn.Encode(m); err != nil {
				log.Print(err)
				continue
			}
		case m := <-tc.decChan:
			switch m.Command {
			case irc.PING:
				pongMessage := &irc.Message{
					Command:       irc.PONG,
					Params:        m.Params,
					Trailing:      m.Trailing,
					EmptyTrailing: m.EmptyTrailing,
				}
				tc.conn.Encode(pongMessage)
			case irc.PRIVMSG:
				tc.RecvChan <- m
			default:
				log.Print(m)
			}
		}
	}
}

func (tc *IRCConn) decoder() {
	for {
		m, err := tc.conn.Decode()
		if err != nil {
			if err == io.EOF {
				log.Print("EOF - reconnecting...")
				tc.connect()
				continue
			}
			log.Print(err)
			continue
		}
		tc.decChan <- m
	}
}

func (tc *IRCConn) connect() (err error) {
	log.Print("connecting to IRC...")

	tc.conn, err = irc.Dial(tc.addr)
	if err != nil {
		return
	}

	passMessage := &irc.Message{
		Command: irc.PASS,
		Params:  []string{tc.pass},
	}
	if err = tc.conn.Encode(passMessage); err != nil {
		return
	}

	nickMessage := &irc.Message{
		Command: irc.NICK,
		Params:  []string{tc.nick},
	}
	if err = tc.conn.Encode(nickMessage); err != nil {
		return
	}

	userMessage := &irc.Message{
		Command:  irc.USER,
		Params:   []string{tc.nick, "0", "*"},
		Trailing: tc.nick,
	}
	if err = tc.conn.Encode(userMessage); err != nil {
		return
	}

	for _, cap := range tc.capabilities {
		capMesage := &irc.Message{
			Command:  irc.CAP,
			Params:   []string{irc.CAP_REQ},
			Trailing: cap,
		}
		if err = tc.conn.Encode(capMesage); err != nil {
			return
		}
	}

	for channel := range tc.channels {
		if err := tc.JoinChannel(channel); err != nil {
			return err
		}
	}

	log.Print("connected to IRC")

	return
}

// JoinChannel makes the bot join a channel
func (tc *IRCConn) JoinChannel(channel string) error {
	joinMessage := &irc.Message{
		Command: irc.JOIN,
		Params:  []string{"#" + channel},
	}
	if err := tc.conn.Encode(joinMessage); err != nil {
		return err
	}

	tc.channels[channel] = struct{}{}

	return nil
}

// LeaveChannel makes the bot leave a channel, unless the channel being left is the last channel
func (tc *IRCConn) LeaveChannel(channel string) error {
	if len(tc.channels) < 1 {
		return nil
	}

	partMessage := &irc.Message{
		Command: irc.PART,
		Params:  []string{"#" + channel},
	}
	if err := tc.conn.Encode(partMessage); err != nil {
		return err
	}

	delete(tc.channels, channel)

	return nil
}
